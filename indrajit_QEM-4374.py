# author: indrajit.dewanjee

# Converting integer to float
def convert_int_to_float(input_int):
    try:
        input_int = int(input_int)
    except ValueError:
        print("Input was not an integer, assigning default value 0 to input")
        input_int = 0
    float_a = float(input_int)
    return float_a


# Addition of string(higher) data type and integer(lower) datatype
def add_str_and_int(input_str, input_int):
    try:
        input_int = int(input_int)
    except ValueError:
        print("Input was not an integer, assigning default value 0 to input")
        input_int = 0
    output = input_str + str(input_int)
    return output


# Print fibonacci series and take value as user input
def fibonacci(n):
    a = 0
    b = 1
    c = 0
    try:
        n = int(n)
    except ValueError:
        print("Input was not an integer, assigning default value 10 to input")
        n = 10

    if n == 1:
        print("0")
    elif n == 2:
        print("0 1")
    else:
        for x in range(0, n):
            print(int(a), end=" ")
            c = a + b
            a = b
            b = c
    print("")


# Game of Thrones...........................
def valar_morghulis(input_str):
    print("a) extract from input => ""one""")
    ans_a = input_str[9:12]
    print(ans_a)

    print("b) convert (a) from ""one"" to => ""One""")
    ans_b = ans_a.capitalize()
    print(ans_b)

    print("c) reverse the input => ""senorhTfOemaG""")
    ans_c = ""
    for c in range(len(input_str) - 1, -1, -1):
        ans_c += input_str[c]
    print(ans_c)

    print("d) reverse the input case => ""gAMEoFtHRONES""")
    ans_d = ""
    for c in input_str:
        if c.islower():
            ans_d += c.upper()
        else:
            ans_d += c.lower()
    print(ans_d)

    print("e) extract caps into one string => ""GOT""")
    ans_e = ""
    for c in input_str:
        if c.isupper():
            ans_e += c
    print(ans_e)


###############################################################################
print("Converting integer to float---")
int_a = input("Enter an integer: ")
print("Converted to float : ", end="")
print(convert_int_to_float(int_a))
print()

print("Addition of string(higher) data type and integer(lower) datatype---")
str_a = input("Enter a string: ")
int_a = input("Enter an integer: ")
print("Added value : ", end="")
print(add_str_and_int(str_a, int_a))
print()

print("Print fibonacci series and take value as user input---")
n = input("Enter the length of Fibonaaci series: ")
fibonacci(n)
print()

print("Game of Thrones...........................")
input_str = input("Enter your GoT string : ")
valar_morghulis(input_str)
